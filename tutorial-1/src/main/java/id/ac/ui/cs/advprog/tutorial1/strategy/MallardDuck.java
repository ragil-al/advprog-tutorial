package id.ac.ui.cs.advprog.tutorial1.strategy;

public class MallardDuck extends Duck {

    public MallardDuck(){
        super.setFlyBehavior(new FlyWithWings());
        super.setQuackBehavior(new Quack());
    }
    
    public void display() {
    	System.out.println("Display mallard");
    }
}
