package tutorial.javari.animal;

public class AnimalForm {
    private final String type;
    private final String name;
    private final double length;
    private final double weight;
    private final String gender;
    private final String condition;

    public AnimalForm(String type, String name, double length,
                      double weight, String gender, String condition) {
        this.type = type;
        this.name = name;
        this.length = length;
        this.weight = weight;
        this.gender = gender;
        this.condition = condition;
    }

    public String getType() {
        return type;
    }

    public String getName() {
        return name;
    }

    public double getLength() {
        return length;
    }

    public double getWeight() {
        return weight;
    }

    public String getGender() {
        return gender;
    }

    public String getCondition() {
        return condition;
    }
}
