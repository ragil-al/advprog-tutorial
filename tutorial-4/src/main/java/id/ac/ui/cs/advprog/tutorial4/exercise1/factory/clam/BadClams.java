package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam;

public class BadClams implements Clams {

    public String toString() {
        return "Bad Clams from Chesapeake Bay";
    }
}
