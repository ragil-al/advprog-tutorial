package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese;

public class SmellyCheese implements Cheese {

    public String toString() {
        return "Smelly Cheese";
    }
}
