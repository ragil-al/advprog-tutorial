package id.ac.ui.cs.advprog.tutorial3.decorator;

import id.ac.ui.cs.advprog.tutorial3.decorator.bread.CrustySandwich;
import id.ac.ui.cs.advprog.tutorial3.decorator.filling.BeefMeat;
import id.ac.ui.cs.advprog.tutorial3.decorator.filling.Cucumber;
import id.ac.ui.cs.advprog.tutorial3.decorator.filling.Lettuce;

public class DecoratorMain {
    public static void main(String[] args) {
        Food food = new Lettuce(new BeefMeat(new Cucumber(new CrustySandwich())));
        System.out.println("Description: " + food.getDescription());
        System.out.println("Cost: " + food.cost());
    }
}
